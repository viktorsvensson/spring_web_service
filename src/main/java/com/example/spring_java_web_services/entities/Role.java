package com.example.spring_java_web_services.entities;

public enum Role {
    ADMIN,
    USER
}
