package com.example.spring_java_web_services.dto;

public record DtoRequest (String username, String password) {
}
