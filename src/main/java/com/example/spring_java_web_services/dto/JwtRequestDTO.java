package com.example.spring_java_web_services.dto;

public record JwtRequestDTO(String username, String password) {
}
