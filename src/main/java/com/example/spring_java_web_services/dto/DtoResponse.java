package com.example.spring_java_web_services.dto;

public record DtoResponse(int id, String username) {
}
