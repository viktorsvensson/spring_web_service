package com.example.spring_java_web_services.services;

import com.example.spring_java_web_services.dto.DtoRequest;
import com.example.spring_java_web_services.dto.DtoResponse;
import com.example.spring_java_web_services.entities.AppUser;
import com.example.spring_java_web_services.repo.AppUserRepo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AppUserService {

    private final AppUserRepo appUserRepo;

    public AppUserService(AppUserRepo appUserRepo){
        this.appUserRepo = appUserRepo;
    }

    public List<DtoResponse> findAll(String contains){
        return appUserRepo
                .findAppUsersByUsernameContaining(contains)
                .stream()
                .map(appUser -> new DtoResponse(appUser.getId(), appUser.getUsername()))
                .toList();

    }

    public DtoResponse insertAppUser(DtoRequest dtoRequest){

        /*
        // Utan särskild konstruktor i entiten
        AppUser appUser = new AppUser();
        appUser.setUsername(dtoRequest.username());
         */

        AppUser existingAppUser = appUserRepo.save(new AppUser(dtoRequest.username(), dtoRequest.username(), null));

        return new DtoResponse(existingAppUser.getId(), existingAppUser.getUsername());
    }


    public DtoResponse findById(int id) {
        AppUser existingAppUser = appUserRepo
                .findById(id)
                .orElseThrow();

        return new DtoResponse(existingAppUser.getId(), existingAppUser.getUsername());
    }

    public DtoResponse updateById(int id, DtoRequest dtoRequest) {

        AppUser existingAppUser = appUserRepo
                .findById(id)
                .orElseThrow();

        if(dtoRequest.username() != null){
            existingAppUser.setUsername(dtoRequest.username());
        }

        AppUser createdAppUser = appUserRepo.save(existingAppUser);
        return new DtoResponse(createdAppUser.getId(), createdAppUser.getUsername());

    }
}
