package com.example.spring_java_web_services.services;

import com.example.spring_java_web_services.entities.AppUser;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class AuthServices {

    public static boolean isNamedGunnar(){

        AppUser appUser = (AppUser) SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal();

        return appUser.getUsername().equalsIgnoreCase("Gunnar");

    }


    public boolean idMatchesHour(int id) {
        return (id == LocalDateTime.now().getHour() - 8);
    }
}
