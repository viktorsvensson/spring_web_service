package com.example.spring_java_web_services.services;

import com.example.spring_java_web_services.dto.Todo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.List;

@Service
public class TodoService {

    //@Autowired
    private final WebClient webClient;

    public TodoService(WebClient webClient) {
        this.webClient = webClient;
    }

    public List<Todo> findAll() {
        return webClient
                .get()
                .uri("/todos")
                .exchangeToFlux(clientResponse -> clientResponse.bodyToFlux(Todo.class))
                .buffer()
                .blockLast();
    }

    public Todo insertTodo(Todo todo) {
        return webClient
                .post()
                .uri("/todos")
                .body(Mono.just(todo), Todo.class)
                .exchangeToMono(clientResponse -> clientResponse.bodyToMono(Todo.class))
                .block();


    }
}
