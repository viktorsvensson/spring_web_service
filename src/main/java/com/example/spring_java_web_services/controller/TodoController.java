package com.example.spring_java_web_services.controller;

import com.example.spring_java_web_services.dto.Todo;
import com.example.spring_java_web_services.services.TodoService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/todo")
@SecurityRequirement(name = "JwtAuth")
public class TodoController {

    private final TodoService todoService;

    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }

    @GetMapping
    public List<Todo> findAll(){
        return todoService.findAll();
    }

    @PostMapping
    public Todo insertTodo(@RequestBody Todo todo){
        return todoService.insertTodo(todo);
    }

}
