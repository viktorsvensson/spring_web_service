package com.example.spring_java_web_services.controller;

import com.example.spring_java_web_services.dto.DtoRequest;
import com.example.spring_java_web_services.dto.DtoResponse;
import com.example.spring_java_web_services.services.AppUserService;
import com.example.spring_java_web_services.services.AuthServices;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/appuser")
//@SecurityRequirement(name = "JwtAuth")
//@Secured("ROLE_ADMIN")
public class AppUserController {

    private final AppUserService appUserService;
    private final AuthServices authServices;

    public AppUserController(AppUserService appUserService, AuthServices authServices){
        this.appUserService = appUserService;
        this.authServices = authServices;
    }

    @GetMapping
    //@PreAuthorize("@authServices.isNamedGunnar()")
    public List<DtoResponse> findAll(
            @RequestParam(required = false, defaultValue = "") String usrcont
            )
    {
        return appUserService.findAll(usrcont);
    }

    @GetMapping("/{id}")
    @PreAuthorize("@authServices.idMatchesHour(#id)")
    public DtoResponse findById(@PathVariable int id){
        return appUserService.findById(id);
    }

    @PostMapping
    public DtoResponse insertAppUser(@RequestBody DtoRequest dtoRequest){
        return appUserService.insertAppUser(dtoRequest);
    }

    @PatchMapping("/{id}")
    public DtoResponse  uppdateById(
            @PathVariable int id,
            @RequestBody DtoRequest dtoRequest
    ){
        return appUserService.updateById(id, dtoRequest);
    }
}
