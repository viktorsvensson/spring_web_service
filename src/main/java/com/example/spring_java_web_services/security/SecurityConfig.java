package com.example.spring_java_web_services.security;

import com.example.spring_java_web_services.services.AuthServices;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
//@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
public class SecurityConfig {

    private UserDetailsService userDetailsService;
    private final JwtRequestFilter jwtRequestFilter;
    private final AuthServices authServices;

    public SecurityConfig(
            UserDetailsService userDetailsService, JwtRequestFilter jwtRequestFilter, AuthServices authServices) {
        this.userDetailsService = userDetailsService;
        this.jwtRequestFilter = jwtRequestFilter;
        this.authServices = authServices;
    }

    @Bean(name = "passwordEncoder")
    PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }


    @Bean(name = "filterChain")
    SecurityFilterChain filterChain(HttpSecurity httpSecurity) throws Exception {

        httpSecurity
                .csrf().disable()
                .authorizeRequests(auth -> auth
                        .requestMatchers("/api/auth/**", "/swagger-ui/**", "/v3/api-docs/**").permitAll()
                        .requestMatchers("/api/todo/**").authenticated()
                        .requestMatchers("/api/appusers/{id}").access("@authServices.idMatchesHour(#id)")
                        .anyRequest().authenticated())
                .userDetailsService(userDetailsService)
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .httpBasic().disable();

        httpSecurity.addFilterBefore(jwtRequestFilter,
               UsernamePasswordAuthenticationFilter.class);

        return httpSecurity.build();

    }

}
