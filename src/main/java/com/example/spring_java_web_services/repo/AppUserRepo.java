package com.example.spring_java_web_services.repo;

import com.example.spring_java_web_services.entities.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AppUserRepo extends JpaRepository<AppUser, Integer> {

    List<AppUser> findAppUsersByUsernameContaining(String containing);

    Optional<AppUser> findAppUsersByUsernameIgnoreCase(String username);

}

