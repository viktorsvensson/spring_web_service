package com.example.spring_java_web_services;

import com.example.spring_java_web_services.entities.AppUser;
import com.example.spring_java_web_services.entities.Role;
import com.example.spring_java_web_services.repo.AppUserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;

@SpringBootApplication
public class SpringJavaWebServicesApplication implements CommandLineRunner {

    @Autowired
    AppUserRepo appUserRepo;

    @Autowired
    PasswordEncoder passwordEncoder;

    public static void main(String[] args) {
        SpringApplication.run(SpringJavaWebServicesApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        appUserRepo.save(new AppUser("gunnar", passwordEncoder.encode("fika!"), List.of(Role.USER)));
        appUserRepo.save(new AppUser("alva", passwordEncoder.encode("fika!"), List.of(Role.ADMIN)));
        appUserRepo.findAppUsersByUsernameIgnoreCase("gunnar").ifPresent(System.out::println);
    }


}
